/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.exemplo6.test;

import br.com.senac.exemplo6.NotaAluno;
import org.junit.Test;
import static org.junit.Assert.*;

public class NotaAlunoTest {
    
    public NotaAlunoTest() {
    }
    
    @Test
    public void alunoDeveSerAprovadoComNotaMaoirQue7 (){
        double nota = 7;
        String  resultado = NotaAluno.situacao(nota);
        assertEquals(NotaAluno.APROVADO , resultado);
    }
    
    @Test
    public void alunoDeRecuperacaoComNotaAcimaDe4EMenorQue6(){
        double nota = 5;
        String resultado = NotaAluno.situacao(nota);
        assertEquals(NotaAluno.RECUPERACAO, resultado);
    }
    
    @Test
    public void alunoReprovadoComNotaAbaixoDe4(){
        double nota = 3;
        String resultado = NotaAluno.situacaoReprovado(nota);
        assertEquals(NotaAluno.REPROVADO, resultado);
    }
  
}
